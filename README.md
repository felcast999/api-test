<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>


## API TEST

## Pasos para instalar:



- Clonar respositorio
- Crear variables de entorno `php artisan key:generate`
- Ejecutar migraciones



## Que se implemento



* Cache
* Interfaces
* Dependency injection
* Collections
* Services
* Singletons
* Pagination
