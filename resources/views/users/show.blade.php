@extends('layouts.app')

@section('content')
<div class="row display-table-row">
            <div class="col-md-2 col-sm-1 hidden-xs display-table-cell v-align box" id="navigation">
                <div class="logo">
                    <a hef="home.html"><img src="http://jskrishna.com/work/merkury/images/logo.png" alt="merkery_logo" class="hidden-xs hidden-sm">
                        <img src="http://jskrishna.com/work/merkury/images/circle-logo.png" alt="merkery_logo" class="visible-xs visible-sm circle-logo">
                    </a>
                </div>
                <div class="navi">
                    <ul>
                        <li class="active"><a href="/"><i class="fa fa-user" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Users</span></a></li>
                        <li><a href="#"><i class="fa fa-tasks" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Workflow</span></a></li>
                        <li><a href="#"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Statistics</span></a></li>
                        <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Setting</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-10 col-sm-11 display-table-cell v-align">
                <!--<button type="button" class="slide-toggle">Slide Toggle</button> -->
                <div class="row">
                    <header>
                        <div class="col-md-7">
                            <nav class="navbar-default pull-left">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target="#side-menu" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                            </nav>
                            <div class="search hidden-xs hidden-sm">
                                <input type="text" placeholder="Search" id="search">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="header-rightside">
                                <ul class="list-inline header-top pull-right">
                                    <li class="hidden-xs"><a href="#" class="add-project" data-toggle="modal" data-target="#add_project">Add Project</a></li>
                                    <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                    <li>
                                        <a href="#" class="icon-info">
                                            <i class="fa fa-bell" aria-hidden="true"></i>
                                            <span class="label label-primary">3</span>
                                        </a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="http://jskrishna.com/work/merkury/images/user-pic.jpg" alt="user">
                                            <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <div class="navbar-content">
                                                    <span>JS Krishna</span>
                                                    <p class="text-muted small">
                                                        me@jskrishna.com
                                                    </p>
                                                    <div class="divider">
                                                    </div>
                                                    <a href="#" class="view btn-sm active">View Profile</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </header>
                </div>


                <div class="user-dashboard">
                   <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <h1>User Detail</h1>
                    </div>

                     <div class="col-lg-12 col-xl-12">

                        <div class="row center-row">
                            
                        <div class="col-lg-11 col-xl-11 ">
                            <div classs="row ">
                                <input type="hidden" id="client_id" value="{{$id}}">
                                <div class="col-lg-4 col-xl-4">
                                    <span><b>User id:</b></span> {{$user["user_id"]}}
                                </div>

                                <div class="col-lg-4 col-xl-4">
                                    <span><b>Identification Number:</b></span> {{$user["identification_number"]}}
                                </div>

                                <div class="col-lg-4 col-xl-4">
                                    <span><b>Mobile Number:</b></span> {{$user["mobile_number"]}}
                                </div>

                                
                                <div class="col-lg-4 col-xl-4">
                                    <span><b>Birth date:</b></span> {{$user["birth_date"]}}
                                </div>

                                <div class="col-lg-4 col-xl-4">
                                    <span><b>Inactive Reason:</b></span> {{($user["inactivate_reason"]=='')?'Aun Activo':$user["inactivate_reason"]}}
                                </div>
                            </div>
                        </div>
                        </div>


                        <div class="row table-row">

                        <div class="col-lg-11 col-xl-11">
                            <table class="table">
                            <img src="{{asset('images/small-spinner.gif')}}" class="spinner hide" alt="">

                                <thead>
                                    <tr>
                                        <th class="text-center">Client Id</th>
                                        <th class="text-center">Transaction Detail</th>
                                        <th class="text-center">Month</th>
                                        <th class="text-center">Year</th>
                                        <th class="text-center">created at</th>

                                    </tr>
                                </thead>

                                <tbody id="table-body">
                                <tr>
                                        <td colspan="6" class="text-center">
                                            <p>Cargando datos...</p>
                                            <p>Esto puede tardar un poco ฅ՞•ﻌ•՞ฅ</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                   

                        </div>

                        <div class="row pagination-row">
                            <div class="col-lg-11 col-xl-11" id="pagination-col">
                         
                            </div>
                        </div>
                      
                    </div>


                   </div>
                </div>
            </div>
        </div>

    </div>

        </div>

    </div>


@endsection

@push('scripts')
<script src="{{asset('/js/users/show.js')}}"></script>

@endpush