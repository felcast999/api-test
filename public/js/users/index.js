
let users=[];
let paginator=[];

$(document).ready(function(){
    $('[data-toggle="offcanvas"]').click(function(){
        $("#navigation").toggleClass("hidden-xs");
    });
    load_data();


    $(document).on("click",".pagination-element",function(e){

        e.preventDefault();
        let url=e.target.href;
        $(".spinner").removeClass('hide');
        load_data(url);

    })

 });
 

 async function get_user_data(url) {
   let _users= await $.get(url);
        paginator=_users.links;
    return _users.data;
}


async function load_data(url='/api/get-users')
{
    users=await get_user_data(url);
    let tr=``;

    for (const i in users) 
    {

        tr=tr+`<tr>
       <td class="text-center">${users[i].user_id}</td>
       <td class="text-center">${users[i].identification_number}</td>
       <td class="text-center">${users[i].mobile_number}</td>
       <td class="text-center">${((users[i].inactivate_reason)=='' || (users[i].inactivate_reason)==null)?'Aun Activo':users[i].inactivate_reason}</td>
       <td class="text-center">${(users[i].created_at==null)?'Sin especificar':users[i].created_at}</td>
       <td class="text-center">
       <a href="/users/${users[i].id}" class='btn btn-default'>
       <i class="fa fa-eye" aria-hidden="true"></i>
       </a></td>

       </tr>
       `;


    }

    $("#table-body").html(tr);
    add_pagination();

}


function add_pagination()
{
    let start_paginator=`<ul class="pagination">`;

    let elements=``;
    for (const i in paginator) {
       
        elements=elements+`
        <li>
        <a  class="pagination-element ${(paginator[i].active==true) && 'active'}" href="${paginator[i].url}">${paginator[i].label.replace("Previous", "").replace("Next", "")}
        </a>
        </li>`;
    }

    let end_paginator=`</ul>`;

    $("#pagination-col").html(start_paginator+elements+end_paginator);
    $(".spinner").addClass('hide');


}