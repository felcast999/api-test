
let transactions=[];
let paginator=[];

$(document).ready(function(){
    $('[data-toggle="offcanvas"]').click(function(){
        $("#navigation").toggleClass("hidden-xs");
    });

    let id=$("#client_id").val();
    load_data(`/api/get-users/${id}`);


    $(document).on("click",".pagination-element",function(e){

        e.preventDefault();
        let url=e.target.href;
        $(".spinner").removeClass('hide');

        load_data(url);

    })

 });
 

 async function get_transaction_data(url) {
   let _transactions= await $.get(url);
        paginator=_transactions.links;
    return _transactions.data;
}


async function load_data(url='/api/get-users/1')
{
    let transactions=await get_transaction_data(url);
    console.log(transactions)
    let tr=``;
    if(Object.keys(transactions).length>0)
    {
        for (const i in transactions) 
        {
    
            tr=tr+`<tr>
           <td class="text-center">${transactions[i].client_id}</td>
           <td class="text-center">${transactions[i].transaction_detail}</td>
           <td class="text-center">${transactions[i].month}</td>
           <td class="text-center">${transactions[i].year}</td>
           <td class="text-center">${transactions[i].created_at}</td>
           </tr>
           `;
    
    
        }

        add_pagination();

    }else
    {
        tr=`<tr>
        <td class="text-center" colspan="5">No hay datos para mostrar</td>
        </tr>
        `;
    }
 

    $("#table-body").html(tr);

}

function add_pagination()
{
    let start_paginator=`<ul class="pagination">`;

    let elements=``;
    for (const i in paginator) {
       
        elements=elements+`
        <li>
        <a  class="pagination-element ${(paginator[i].active==true) && 'active'}" href="${paginator[i].url}">${paginator[i].label.replace("Previous", "").replace("Next", "")}
        </a>
        </li>`;
    }

    let end_paginator=`</ul>`;

    $("#pagination-col").html(start_paginator+elements+end_paginator);
    $(".spinner").addClass('hide');


}