<?php
namespace App\Classes;

use Exception;
use Illuminate\Support\Facades\Http;
use App\Interfaces\Crud;

class UserService implements Crud
{

    private $module="users";
    private $_api_url;
    private $_token;
    private $base_url;

    public function __construct( )
    {
        $this->_api_url=config('app.api_url');
        $this->_token=app('TOKEN');
        $this->base_url="{$this->_api_url}/{$this->module}/{$this->_token}";
    }


    public function index()
    {
      //Set guzzle timeout to 5 minutes allow us to wait for large amount of data
      set_time_limit(300);
      return $response =json_decode(Http::timeout(300)->accept('application/json')->get($this->base_url)->getBody(),true);

    }

    public function show($id)
    {
      $this->base_url="{$this->base_url}/transaction/{$id}";
      return $response =json_decode(Http::timeout(300)->accept('application/json')->get($this->base_url)->getBody(),true);

    }


}