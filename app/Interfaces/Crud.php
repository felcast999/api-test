<?php
namespace App\Interfaces;

interface Crud
{
    public function index();

    public function show($id);
}