<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\UserService;
use App\Helpers\CollectionHelper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Exception;

class UserController extends Controller
{
    private $_user_service;
    private $page_size=10;

    public function __construct(UserService $user_service)
    {
        $this->_user_service=$user_service;
    
    }

    public function index()
    {

        try {
        //Setting the cache time to 10 minutes, this increase the perfomance and speed of the aplication
        $users = Cache::remember('users', 600, function () {
            return $this->_user_service->index();
           });
         
         //creating collection and sorting
          $collect=collect(Cache::get('users'))->sortByDesc('created_at');
 
          $paginated = CollectionHelper::paginate($collect, $this->page_size);
 
          return response()->json($paginated,200);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'error',
                'error' => $e->getMessage()
            ], 500);
        }
 

    }


    public function show($id)
    {

        try {
        //Getting user info with transactions
        $user=collect(Cache::get('users'))->firstWhere('id',$id);
        $user_transactions = Cache::remember("transactions/{$id}", 600, function () use($id) {
            return $user_transactions=$this->_user_service->show($id);

           });

        $collect=collect($user_transactions)->sortByDesc('created_at');

        $log_info=["user"=>$user,"transactions"=>$user_transactions];
        
        //Storing log info
        Log::channel('api-test')->info($log_info);


        $paginated = CollectionHelper::paginate($collect, $this->page_size);

        return response()->json($paginated,200);
    } catch (Exception $e) {
        return response()->json([
            'message' => 'error',
            'error' => $e->getMessage()
        ], 500);
    }

    }



}
